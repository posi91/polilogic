# polilogic

**Proyecto Politecnico Grancolombiano
Elementos de Teoria de la Computacion**

*Integrantes*

Tatiana Parada
Camilo Urrego
Jonathan Posada

Ingenieria de Sistemas

Índice 


Índice	1
Introducción	3
Descripción	3
Funcion	3
Documentación	3
Lógica Proposicional	3
Gramática	4
logic.validate()	4
logic.satisfiable()	5
Objeto de Contexto	5
Condiciones	6









Introducción

Nombre del Programa: polilogic
Lenguaje: python
Versión: 1.0

Descripción

Un módulo de Python para la validación de sentencias proposicionales y lógicas de primer orden.
El uso más sencillo del módulo lógico es verificar la validez de las declaraciones lógicas proposicionales. Para evaluar las declaraciones lógicas proposicionales, se tiene la función logic.validate() o Context.validate(). Las tablas de la verdad también se pueden imprimir con Context.print TruthTable().
Funcion

Documentación
Lógica Proposicional
Las declaraciones de lógica propositiva se pueden validar utilizando el script polilogic.py, lo que significa que su verdad se puede calcular fácilmente y devolver como un boleano de Python. Dependiendo de la situación, el usuario puede ser deseable que una declaración es una verdad para todas las calificaciones para todos los medios posibles (es decir, que la declaración es una tautología), que es cierta para la menos una recomendación (es decir, que la declaración es satisfactoria), o en los casos en que la declaración no es una tautología, imprime una tabla de verdad que muestre las calificaciones que hacen que la declaración sea falsa.
Gramática
Todas las funciones públicas dan una cadena como un argumento que define la declaración a validar. Esta cadena lógica debe ajustarse a la sintaxis especificada por polilogic.py. La gramática está en el sentido de que las declaraciones lógicas son fáciles de construir y fáciles de leer. 
La gramática completa para la lógica propuesta se especifica en la siguiente tabla:
<atom>      ::= [cualquier cadena en este soporte]   
   <formula>    ::= <formula> implica <formula>                 
            | <formula> Por lo tanto <formula>                 
            | <formula> or <formula>                 
            | <formula> and <formula>                 
            | not <formula>                 
            | {<formula>}                    
            | <atom>
Podemos traducir una declaración lógica tradicional a nuestra sintaxis más humana y amigable con el programa:

se convierte en:
not [P] implica {[R] and [Q]} Por lo tanto  [R] or [P]
logic.validate()
El método logic.validate() se usa para evaluar una cadena que representa una declaración lógica especificada en la sintaxis anterior. Esta función se puede llamar directamente desde el módulo lógico sin crear instancias de ningún objeto de clase.
logic.validate() toma una cadena como un argumento y devuelve un bool de Python que es Verdadero y la declaración lógica es una tautología (lo que significa que todas las posibles ventas de los propuestos conducen a la declaración del mar verdadero), y Falso de lo contrario.
 
 
logic.satisfiable()

Una diferencia de lógica. Validar (), lógica. Satisfacible () Devuelve Verdadero si hay al menos una valoración de los átomos. Propuestas en la declaración lógica que lleva a la declaración del mar verdadera. Este método devuelve un valor bool, por lo que para determinar los detalles sobre la valoración de los átomos que llevó a la verdad de esta declaración.

Objeto de Contexto
El objeto de contexto contiene la información de estado que se crea al configurar o evaluar una declaración lógica. También proporciona un mayor control. Con el objeto de contexto puede realizar dos funciones adicionales: imprimir una tabla de verdad y especificar suposiciones antes de llamar a validate().
El objeto de contexto se crea con la llamada de una línea:
c = logic.Context ()
En este punto, puede imprimir una tabla de verdad, que simplemente producirá una columna para cada uno de los propósitos propuestos y otra para la fórmula que el usuario pasa como argumento
c = logic.Context ()
c.printTruthTable ( " [P] y [Q] o [P] y [Q] " , " , " )
# Esto imprime lo siguiente:
# Q, P, {{{[P] y [Q]} o [P]} y [Q]} 
# Falso, Falso, Falso 
# Verdadero, Falso, Falso 
# Falso, Verdadero, Falso 
# Verdadero, Verdadero, Verdadero
 
El objeto Contexto también acepta una serie de fórmulas lógicas como supuestos contra los que se evalúan las llamadas subsiguientes a validate (). Por ejemplo, la declaración:
 
{{{[P] y [Q]} y [R]} y {[S] y [T]}} implica {[Q] y [S]}
 
Se puede simplificar con el uso de supuestos:
lógica de importación
context = logic.Context ()
context.assume ( " [P] y [Q] y [R] " )
context.assume ( " [S] and [T] " )
  imprime str (context.validate ( " [Q] and [S] " ))
  # imprime "True"
Con la última llamada a context.validate (), el objeto de contexto construirá la oración más larga que incluye la implicación de todas las suposiciones que conoce. En otras palabras, todas las suposiciones están compuestas en una conjunción. La fórmula en el método validar () luego se combinará con una implicación, y la fórmula resultante se evaluará, con su verdad devuelta por validate().
 
Condiciones
Los términos son objetos que pueden servir como un argumento para una función de predicado. El dominio para los términos se puede especificar en un objeto de contexto con una llamada a Context.setTermDomain().
Context.setTermDomain() toma un solo argumento, una lista de Python.
Por ejemplo:
c.setTermDomain ([ " apple " , " orange " , " banana " , " zanahoria " ))
 
Establece el dominio en cuatro frutas y verduras. Pero como el Contexto puede tomar cualquier lista de Python, no se limita a un solo cadenas. Un rango finito para enteros se puede crear fácilmente usando el rango de funciones() de Python:
c.setTermDomain ( rango ( 20 ))
 
Funciones de Predicado

Las funciones de predicado se pueden definir implícitamente o explícitamente. Las funciones de predicción definidas de forma implícita son aquellas que se agregan a una función assume() o a assumePredicate (). Al hacer esto, el usuario no tiene que escribir explícitamente una función y registrarse con el Contexto, lo que se puede ahorrar algo de tiempo para modelos simples:

context.assumePredicate("orange colored(naranja)")



Descripción

Un módulo de Python para la validación de sentencias proposicionales y lógicas de primer orden.
