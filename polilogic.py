import lex
import yacc
import re
import copy

t_ignore = ' \t'

_predPattern = re.compile("(?P<statement>.+)(\((?P<args>.+)\))")
        
        
def t_newline(t):
    r'\n+'
    t.lexer.lineno += t.value.count("\n")

def t_error(t):
    print("Illegal character '%s'" % t.value[0])
    t.lexer.skip(1)


def validate(sentence):
    ''' 
      Devuelva verdadero si la oración es una tautología en lógica proposicional
    '''
    context = Context()
    return context.validate(sentence)

def satisfiable(sentence):
    '''
    Devuelva verdadero si una oración es satisfactoria en lógica proposicional. es decir.
     satisfactorio ("[átomo proposicional] y {[otro átomo] implica que no [un tercer átomo]}") devuelve Verdadero
    '''
    context = Context()
    return context.satisfiable(sentence)

class LogicError(UserWarning):
    ''' 
   Todos los errores generados por este módulo son LogicErrors y son sintaxis
     Errores o referencias a términos no vinculados que no están en el dominio de primer orden
    '''
    
    def __init__(self, message):
        super(LogicError, self).__init__(message)

class UnaryOp:
    expression = None
    
class FirstOrderUnaryOp(UnaryOp):
    
    boundVar = None
    
    def getBoundVariablePreds(self):
        '''
        Devuelve una lista de referencias de predicado a variables unidas        '''
        refs = []
        self._getBoundVariablePredsRecursive(refs, self.expression)
        return refs
    
    def _getBoundVariablePredsRecursive(self, refs, expression):
        '''
        Busca recursivamente los predicados que se refieren a variables encuadernadas
        '''
        
        if(isinstance(expression, Predicate)):
            if(expression.argList is not None and self.boundVar in expression.argList):
                refs.append(expression)
        elif(isinstance(expression, UnaryOp)):
            self._getBoundVariablePredsRecursive(refs, expression.expression)
        elif(isinstance(expression, BinaryOp)):
            self._getBoundVariablePredsRecursive(refs, expression.left)
            self._getBoundVariablePredsRecursive(refs, expression.right)
        
        
class BinaryOp:
    # no op
    left = None
    right = None
    
class PredicateFunction:
    
    def __init__(self, arity, function):
        self.arity = arity
        self.function = function

class Context:
    '''
    Proporciona un contexto para el cual se pueden validar las declaraciones lógicas.
    
     Las declaraciones de lógica proposicional se pueden validar fácilmente sin un objeto de contexto.
     Las declaraciones de lógica proposicional se pueden validar llamando a logic.validate ("declaración"), pero
     sentencias lógicas más complejas, como aquellas que involucran predicados lógicos de primer orden y
     Los cuantificadores de primer orden (existen, todos), deben validarse dentro de un contexto.
    '''
    
    def __init__(self):
        
        # list of objects for the first-order logic term domain
        self.domain = []
        
        # propositional atoms or predicates as a dictionary
        # key is the stringified version of the predicate
        self.predicates = {}
        
        # predicate invocations that we know to be true
        # or that we have previously calculated to be true
        self.predicateAssumptions = {}
        
        # logical statements assumed to be true
        self.assumptions = []
        
        # explicitly defined PredicateFunctions -- see the PredicateFunction class
        self.predicateFunctions = {}

    def _isSatisfiable(self, sentence, printTruthTable=False, truthTableSep="\t"):
        parseTree = self.parse(sentence)
        if(printTruthTable):
            self._printTruthTable(parseTree, truthTableSep)
        rows = self.getTruthRows(parseTree)
        return rows >= 1

    def isTautology(self, sentence, printTruthTable=False, truthTableSep="\t"):
        # get number of unique predicates
        # produce 2^num_preds rows for a truth table
        # set predicate values based on above
        # evaluate sentence
        
        parseTree = self.parse(sentence)
        if(printTruthTable):
            self._printTruthTable(parseTree, truthTableSep)
        rows = self.getTruthRows(parseTree)
        return len(rows) == sum(rows)

    def clear(self):
        self.predicates.clear()
        self.assumptions.clear()
        self.predicateFunctions.clear()
        self.predicateAssumptions.clear()
        self.domain = None
        
    def assume(self, sentence):
        ''' Supongamos que una oración lógica es verdadera.
         Esto afectará la evaluación de futuro.
         validación () llamadas, y se puede deshacer con
         una llamada para borrar ()
        '''
        self.assumptions.append(sentence)
        
    def assumePredicate(self, predicate):
        if(predicate[0] == '[' or predicate[len(predicate) - 1] == ']'):
            raise LogicError("los predicados no deben estar envueltos en [] con asumir predicado")
#        self.assume("[" + predicate + "]")
        # keep this
        self.predicateAssumptions[predicate] = True
        
    def _replaceArg(self, expression, replaceThis, replaceWith):
        # to do: recursively find predicates, and replace their arguments that include replaceThis with replaceWith
        expr = expression;
        if(isinstance(expression, BinaryOp)):
            self._replaceArg(expression.left, replaceThis, replaceWith)
            self._replaceArg(expression.right, replaceThis, replaceWith)
            expression.left = self._checkForStoredPredicate(expression.left)
            expression.right = self._checkForStoredPredicate(expression.right)
        elif(isinstance(expression, UnaryOp)):
            self._replaceArg(expression.expression, replaceThis, replaceWith)
            expression.expression = self._checkForStoredPredicate(expression.expression)
        elif(isinstance(expression, Predicate)):
            argList = expression.argList
            if(argList is not None):
                for idx in range(len(argList)):
                    item = argList[idx]
                    if(item == replaceThis):
                        argList[idx] = replaceWith
            expr = self._checkForStoredPredicate(expression)
        return expr
                        
    def _checkForStoredPredicate(self, expression):
        ''' Busque una versión ya almacenada de este predicado (expresión),
         y si ya ha sido almacenado, devuelva esa versión. De lo contrario volver
         sí mismo. Esto mantendrá de duplicar las instancias de Predicado que tienen
         La misma firma y argumentos.'''
        
        if(isinstance(expression, Predicate)):
            s = expression.getFullStatement()
            if(s in self.predicates.keys() and self.predicates[s] is not None):
                return self.predicates[s]
        return expression
        
    def setPredicateFunction(self, function, arity, predString=None):
        ''' Proporcionar una función para definir los valores de verdad para el símbolo de predicado
             sobre los valores en el dominio. Esto requiere que se establezca un dominio.
             a través de setTermDomain
        '''
        
        assert arity > 0
        
        s = predString
        if(s is None):
            s = function.__name__
        self.predicateFunctions[s] = PredicateFunction(arity, function)
        
    def setTermDomain(self, domain_array):
        '''Especifique un término dominio como una lista para las declaraciones lógicas de primer orden.
         Esto puede ser una lista de cualquier tipo, pero cada elemento del dominio debe
         tiene un valor único cuando se lanza como una cadena. En cualquier momento una función de predicado.
         se llama se puede invocar con la versión de cadena del objeto
         en el dominio '''
         # Copia la matriz, hazla una matriz de cadenas.
        stringified = []
        self.predicateAssumptions.clear()
        for x in domain_array:
            stringified.append(str(x))
        self.domain = stringified
        
    def forgetPredicateInvocations(self):
        ''' 
        VAlidate recordará cuáles invocaciones de predicado fueron verdaderas (con las cuales
         argumentos una función predicada era verdadera). Si el contexto ha cambiado para
         Por alguna razón desconocida para este objeto de contexto, debe llamar a este método para
         que las funciones de predicado se invocan de nuevo
        '''
        
        self.predicateAssumptions.clear()
        
    def _buildPredicateAssumptionsFromFunctions(self):
        # enumerate all values in the domain, adding predicateAssumptions
        # if the value in the domain is true for the function
        for predName in self.predicateFunctions.keys():
            fn = self.predicateFunctions[predName]
            
            # produce an array for every possible combination
            # of arguments in this arity, then execute this
            # function over that array, and if it is true,
            # then add the predicateAssumption
            n = len(self.domain)
            a = fn.arity
            
            # total number of possible invocations: n^a
            numCombinations = n ** a
            for i in range(0, numCombinations):
                # i represents the ith argument pair
                args = []
                # now build the argument pair
                argIndices = [0] * a
                # convert to a radix of the domain length
                _toDomainRadix(i, n, argIndices, 0)
                for k in range(len(argIndices)):
                    args.append(self.domain[argIndices[k]])
                # the arguments are built, now see if we've computed their truth
                # before
                stringified = predName + "(" + ",".join(args) + ")"
                if(not stringified in self.predicateAssumptions.keys() and
                   fn.function(*args)):
                    self.assumePredicate(stringified)
    
    def printTruthTable(self, sentence, separator="\t"):
        ''' 
        Imprime una tabla de verdad que incluye todos los predicados o átomos proposicionales.
         y toda la frase. Tenga en cuenta que la tabla de verdad no incluye ninguna
         otras subfórmulas.
        
         Especifique el separador con un segundo argumento (el valor predeterminado es un carácter de tabulación).
        '''
        
        self.predicates.clear()
        
       
        if(len(self.predicateFunctions) > 0):
            self._buildPredicateAssumptionsFromFunctions()
        
        newSentence = " and ".join("{" + str(x) + "}" for x in self.assumptions)
        
        if(len(newSentence) == 0):
            parseTree = self.parse(sentence)
            return self._printTruthTable(parseTree, separator)
        
        newSentence = "{" + newSentence + "} implies {" + sentence + "}"
        parseTree = self.parse(newSentence)
        return self._printTruthTable(parseTree, separator)
    
    def satisfiable(self, assertion, printTruthTable=False, truthTableSep="\t"):
        '''
        Devuelva verdadero si hay al menos una valoración de los predicados o átomos proposicionales
         Eso hace que esta afirmación sea cierta.
        '''
        
        self.predicates.clear()
        
     
        if(len(self.predicateFunctions) > 0):
            self._buildPredicateAssumptionsFromFunctions()
        
        newSentence = " and ".join("{" + str(x) + "}" for x in self.assumptions)
        
        if(len(newSentence) == 0):
            return self._isSatisfiable(assertion, printTruthTable, truthTableSep)
        
        newSentence = "{" + newSentence + "} implies {" + assertion + "}"
        return self._isSatisfiable(newSentence, printTruthTable, truthTableSep)
        
    
    def validate(self, assertion, printTruthTable=False, truthTableSep="\t"):
        '''
        Evaluar una afirmación de la lógica proposicional o de primer orden. Opcional
         los argumentos permiten que la persona que llama especifique si una tabla de verdad debería ser
         Impreso, y con qué cadena como separador entre valores.
        
         Devuelve True si la afirmación es una tautología, de lo contrario, es false.
        '''
        
        self.predicates.clear()

        if(len(self.predicateFunctions) > 0):
            self._buildPredicateAssumptionsFromFunctions()
        
        # construct a new sentence that is the conjunction of all assumptions
        newSentence = " and ".join("{" + str(x) + "}" for x in self.assumptions)
        
        if(len(newSentence) == 0):
            return self.isTautology(assertion, printTruthTable, truthTableSep)
        
        newSentence = "{" + newSentence + "} implies {" + assertion + "}"
        return self.isTautology(newSentence, printTruthTable, truthTableSep)
    
    def _printTruthTable(self, parseTree, separator):
       # hacer una matriz temporal de los predicados no asumidos; es decir, el predicado
         # por lo que no conocemos su verdad. Estos son todos los predicados en self.predicates
         # pero no incluye los predicados en self.predicateAssumptions, ya que
         # son todos verdaderos
        unknownPreds = copy.copy(self.predicates)
        
        for key in self.predicateAssumptions.keys():
            p = Predicate(key, self.domain)
            p.truth = True
            self.predicates[key] = p
            if(key in unknownPreds):
                del unknownPreds[key]
                
     
        for key in self.predicates.keys():
            p = self.predicates[key]
            if(p.argList is not None):
                for term in p.argList:
                    if(not term in self.domain):
                        raise LogicError("WARNING: Term '" + str(term) + "' not in domain " + str(self.domain))

        numPreds = len(self.predicates)
        numUnknownPreds = len(unknownPreds)
        rows = 2 ** numUnknownPreds
        header = ""
        for k in range (0, numPreds):
            header += str(self.predicates.keys()[k]) + separator
            
        header += str(parseTree)
        print header
        
        for i in range (0, rows):
            string = ""
            for k in range (0, numUnknownPreds):
                # get the predicate and set its truth
                pred = unknownPreds[unknownPreds.keys()[k]]
                # bit shift to get a boolean value
                pred.truth = (bool(1 << k & i))
            
            for k in range (0, numPreds):
                pred = self.predicates[self.predicates.keys()[k]]
                string += str(pred.truth) + separator
            string += str(parseTree.isTrue())
            print string
            
    def getTruthRows(self, parseTree):
        '''
        Devuelva una lista de valores de verdad (uno para cada fila que se calcula) para
         Cada valoración de átomos proposicionales o invocaciones de predicados.
        '''
        
        rowTruth = []
        
       
        unknownPreds = copy.copy(self.predicates)
       )
        for key in self.predicateAssumptions.keys():
            p = Predicate(key, self.domain)
            p.truth = True
            self.predicates[key] = p
            if(key in unknownPreds):
                del unknownPreds[key]
        
        # provide any warnings for out-of-domain terms
        for key in self.predicates.keys():
            p = self.predicates[key]
            if(p.argList is not None):
                for term in p.argList:
                    if(not term in self.domain):
                        raise LogicError("WARNING: Term '" + str(term) + "' not in domain " + str(self.domain))
        
        numPreds = len(self.predicates)
        numUnknownPreds = len(unknownPreds)
        rows = 2 ** numUnknownPreds
        
        for i in range (0, rows):
            string = ""
            for k in range (0, numUnknownPreds):
                # get the predicate and set its truth
                pred = unknownPreds[unknownPreds.keys()[k]]
                # bit shift to get a boolean value
                pred.truth = (bool(1 << k & i))
            
            for k in range (0, numPreds):
                pred = self.predicates[self.predicates.keys()[k]]
                string += str(pred.truth) + "\t"
            rowTruth.append(parseTree.isTrue())
        return rowTruth
    
    def parse(self, sentence):
        ''' 
        Realice el análisis real (lex / yacc), defina y use la gramática para
         Lógica proposicional y de primer orden.
        
         Crea un árbol de análisis y devuelve la raíz (un operador con el precedente más bajo).
        '''
        
        # build the lexer
        
        # eclipse may warn you that this is an unused variable, but lex will use it
        tokens = (
                  'LBRACE', 'RBRACE', 'AND', 'OR', 'IMPLIES', 'NOT', 'ALL', 'EXISTS',
                  'PREDICATE', 'BINDING'
                  )
        
        #### DEFINE EVERY TOKEN MATCH AS A FUNCTION ###
        # this has the important side-effect of being able
        # to strictly control order of lexical matching
        
        def t_LBRACE (t): 
            r'\{'
            return t
        def t_RBRACE (t): 
            r'\}'
            return t
        def t_PREDICATE(t): 
            '\[[^\]]+\]'
            return t
        
        def t_AND(t):
            r'and'
            return t
        def t_OR(t):
            r'or'
            return t
        def t_IMPLIES(t):
            r'implies|therefore'
            return t
        
        def t_NOT(t):
            r'not'
            return t
        
        def t_ALL(t):
            r'all'
            return t
        
        def t_EXISTS(t):
            r'exists'
            return t
        
        def t_BINDING(t):
            r'[a-zA-Z_][a-zA-Z_0-9]*'
            return t
            
        # eclipse will warn you that this is an unused variable, but yacc/lex will use it
        precedence = (
                      ('left', 'IMPLIES'),
                      ('left', 'OR', 'AND'),
                      ('left', 'EXISTS', 'ALL'),
                      ('left', 'NOT'),
                      ('left', 'PREDICATE')
                      )
        
        # Build the lexer
        # again, eclipse will warn about this being an unused variable, but yacc will use it
        lexer = lex.lex()
        
        
        def p_expression_not(p):
            'expression : NOT expression'
            p[0] = Negation(p[2])
                
        def p_expression_forall_pred(p):
            'expression : ALL BINDING expression'
            p[0] = All(self, str(p[2]), self.domain, p[3])
            preds = p[0].getBoundVariablePreds()
            for x in preds:
                if(x.getFullStatement() in self.predicates.keys()):
                    del self.predicates[x.getFullStatement()]
            
        def p_expression_exists_pred(p):
            'expression : EXISTS BINDING expression'
            p[0] = Exists(self, str(p[2]), self.domain, p[3])
            preds = p[0].getBoundVariablePreds()
            for x in preds:
                if(x.getFullStatement() in self.predicates.keys()):
                    del self.predicates[x.getFullStatement()]
        
        def p_expression_implies(p):
            'expression : expression IMPLIES expression'
            p[0] = Implication(p[1], p[3])
            
            
        def p_expression_and(p):
            'expression : expression AND expression'
            p[0] = Conjunction(p[1], p[3])
            
        def p_expression_or(p):
            'expression : expression OR expression'
            p[0] = Disjunction(p[1], p[3])
        
        def p_expression_group(p):
            'expression : LBRACE expression RBRACE'
            p[0] = p[2]
            
#        def p_expression_term(p):
#            'expression : predicate'
#            p[0] = p[1]
            
        def p_predicate(p):
            'expression : PREDICATE'
            
            stringified = str(p[1])[1:len(str(p[1])) - 1]
            
            if(stringified in self.predicates):
                p[0] = self.predicates[stringified]
            else:
                p[0] = Predicate(stringified, self.domain)
                self.predicates[stringified] = p[0]
                if(stringified in self.predicateAssumptions):
                    p[0].truth = True
                
        # Error rule for syntax errors
        def p_error(p):
            # get the position
            if p is None: return
            pos = p.lexpos
            # get the nth line
            lines = p.lexer.lexdata.split("\n");
            inp = lines[p.lexer.lineno - 1]
            
            hint = "Unexpected operator or predicate not wrapped in []"
            
            if(inp[pos - 1] == '['):
                hint = "no matching closing bracket ']' after this point"
            if(inp[pos - 1] == '{'):
                hint = "no matching closing brace '}' after this point"
            if(inp[pos] == '}'):
                hint = "no matching opening brace '{' before this point"
            if(inp[pos] == ']'):
                hint = "no matching opening bracket '[' before this point"
            
            raise LogicError("Syntax error at line " + str(p.lexer.lineno) + ", character " + str(pos) + ":\n" + inp + "\n" + (pos * " ") + "^ " + hint);
        
        parser = yacc.yacc("LALR",0)

        parseTree = parser.parse(sentence)
        
        # return the outer-most (lowest priority) operator in this sentence -- it serves as the
        # root to the parse tree
        return parseTree

class Disjunction(BinaryOp):

    def __init__(self, exprLeft, exprRight):
        self.left = exprLeft
        self.right = exprRight
    
    def isTrue(self):
        return (self.left.isTrue() or self.right.isTrue()) 

    def __str__(self):
        return "{" + str(self.left) + " or " + str(self.right) + "}"

class Conjunction(BinaryOp):

    def __init__(self, exprLeft, exprRight):
        self.left = exprLeft
        self.right = exprRight
    
    def isTrue(self):
        return (self.left.isTrue() and self.right.isTrue()) 

    def __str__(self):
        return "{" + str(self.left) + " and " + str(self.right) + "}"

class Predicate:
    '''
   Un objeto Predicado puede representar un átomo proposicional (que es equivalente
     a una función de predicado lógico de primer orden con aridad 0), o un primer orden
     Invocación de la función de predicado lógico (con aridad positiva). La declaración
     debe incluir el nombre del predicado, más un paréntesis que incluya una comu-
     lista separada de argumentos si la función de predicado tiene una aridad positiva.
    '''

    def __init__(self, statement, domain):
        '''
        Constructor
        '''
        self.statement = statement
        self.argList = None
        self.domain = domain
        self._parseArguments(statement)
        self.truth = False
    
    def _parseArguments(self, statement):
        result = _predPattern.match(statement)
        if(result):
            self.statement = result.group("statement")
            #        self.argList = result.group("args").split(",")
            argList = result.group("args").split(",")
            for i in range(len(argList)):
                argList[i] = argList[i].strip()
            self.argList = argList
        else:
            self.statement = statement
        
    def isTrue(self):
        '''
        NB: independientemente de las funciones de predicado definidas explícitamente, este objeto Predicate
         Tiene una verdad estática, que se establece mediante la función de validación.
        '''
        if(self.argList is not None):
            for term in self.argList:
                if(not term in self.domain):
                    raise LogicError("WARNING: Term '" + str(term) + "' not in domain " + str(self.domain))
        return self.truth
    
    def getFullStatement(self):
        '''
       Devuelva la versión de cadena completa de este Predicado, que incluye el
         nombre y sus argumentos (separados por comas, envueltos entre paréntesis).
        '''
        s = self.statement
        if(self.argList is not None):
            s = s + "(" + (",".join(self.argList)) + ")"
        return s
    
    def __str__(self):
        return "[" + self.getFullStatement() + "]"


class Negation(UnaryOp):
    
    def __init__(self, expr):
        self.expression = expr
        
    def isTrue(self):
        return not self.expression.isTrue();

    def __str__(self):
        return "{not " + str(self.expression) + "}"

class Exists(FirstOrderUnaryOp):
    
    def __init__(self, context, boundVariable, domain, expression):
        self.context = context
        self.boundVar = boundVariable
        self.expression = expression
        # go through the expression and remove any predicates that reference bound variables
        # from the predicates array
        self.domain = domain
        
    def isTrue(self):
        # this is the hard part
        # need to loop through each variable in the domain, asking if there is a variable, x, that satisfies the domain
        if(self.domain is None):
            return False
        for x in self.domain:
            exp = copy.deepcopy(self.expression)
            exp = self.context._replaceArg(exp, self.boundVar, x)
            if(exp.isTrue()): 
                return True
        return False
       
    def __str__(self):
        return "exists " + self.boundVar + " {" + str(self.expression) + "}"

class All(FirstOrderUnaryOp):
    
    def __init__(self, context, boundVariable, domain, expression):
        self.context = context
        self.boundVar = boundVariable
        self.expression = expression
        # go through the expression and remove any predicates that reference bound variables
        # from the predicates array
        self.domain = domain
        
    def isTrue(self):
        # this is the hard part
        # need to loop through each variable in the domain, asking if there is a variable, x, that satisfies the domain
        if(self.domain is None):
            return False
        for x in self.domain:
            exp = copy.deepcopy(self.expression)
            exp = self.context._replaceArg(exp, self.boundVar, x)
            if(not exp.isTrue()): 
                return False
        return True
       
    def __str__(self):
        return "all " + self.boundVar + " {" + str(self.expression) + "}"
    

class Implication (BinaryOp):
    
    def __init__(self, expr1, expr2):
        self.left = expr1;
        self.right = expr2;
    
    def isTrue(self):
        if(self.left.isTrue() and (not self.right.isTrue())):
            return False
        return True
    
    def __str__(self):
        return "{" + str(self.left) + " implies " + str(self.right) + "}"

def _toDomainRadix(m, n, arr, idx):
    '''
    Takes an integer, m, and populates an array with integers from
    0 to n-1 that represent the result of converting m to base-n radix.
    '''
    if(n == 1): return
    if(m < n):
        arr[idx] = m
    else:
        _toDomainRadix(int(m / n), n, arr, idx + 1)
        arr[idx] = (m % n)
